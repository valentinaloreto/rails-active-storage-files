class BlogPostsController < ApplicationController
  before_action :set_post, only: [:edit, :update, :destroy]

  def index
    @posts = BlogPost.all
  end

  def new
    @post = BlogPost.new
  end

  def create
    # create db entry using the blog_post_params, which automatically creates attached the entries for main and uploads
    @post = BlogPost.new(blog_post_params)

    # check if there is a main image attached; if so, check that it has a valid extension; if not, delete
    if @post.main_picture.attached?
      unless @post.is_attachment_valid_image?
        @post.main_picture.purge
      end
    end

    # check that there are uploads attached; if so, check each one and see if it's the right format; if not, delete
    if  @post.uploads.attached?
      @post.uploads.each do |upload|
        unless upload.blob.content_type == 'application/pdf' || upload.blob.content_type.start_with?('image/')
          upload.purge
        end
      end
    end

    # persist the post and redirect to the index view
    if @post.save
      redirect_to blog_posts_path
    end

  end

  def edit
    @post
  end

  def update

  end

  def destroy

  end

  private

  def blog_post_params
    params.require(:blog_post).permit(:title, :content, :main_picture, uploads: [])
  end

  def set_post
    @post = BlogPost.find(params[:id])
  end
end

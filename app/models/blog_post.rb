class BlogPost < ApplicationRecord
  has_one_attached :main_picture # one-to-one relationship
  has_many_attached :uploads # one-to-many relationship

  def is_attachment_valid_image?
    main_picture.blob.content_type.start_with?('image/')
  end

  # def is_attachment_valid_pdf?()
  #   main_picture.blob.content_type == 'application/pdf'
  # end
end

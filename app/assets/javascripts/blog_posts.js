document.addEventListener("turbolinks:load", function() {
  // VARIABLES
  let form = document.querySelector("#new-post-form");
  let title = document.querySelector('#blog_post_title');
  let content = document.querySelector('#blog_post_title');
  let main_image = document.querySelector('#blog_post_main_picture');
  let newsubmit = document.querySelector('#new_submit');

  // FUNCTIONS

  function validateTitle(){
    if (title.value.length < 2) {
      alert('Title needs to be longer');
      return false;
    } else {
      return true;
    }

  }

  function validateContent(){
    if (content.value.length < 10) {
      alert(content.value.length);
      alert('Content needs to be longer');
      return false
    } else {
      return true;
    }
  }

  function validateMainImage(){
    let raw_image_files = ["tiff","jpeg","gif","png"];

    let image_name_from_user = main_image.value.split(".");

    if ( image_name_from_user == "" ) {
      return true;
    } else {
      let extension = image_name_from_user[image_name_from_user.length-1]
      if (extension) {
        let aux = raw_image_files.filter(function(el){return el == extension})
        if (aux.length > 0) {
          return true;
        } else {
          alert('not valid extension');
          return false;
        }
      }
    }


  }


  // EVENT LISTENERS

  newsubmit.addEventListener('click', function(e){
    e.preventDefault();
    let validatedtitle = validateTitle();
    let validatedcontent = validateContent();
    let validatedmainimage = validateMainImage();
    if ( validatedtitle && validatedcontent && validatedmainimage) {
      form.submit();
    }
  });

});





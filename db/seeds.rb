# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


BlogPost.create({
  title: 'This is title #1',
  content: 'lorem ipsumlorem ipsumlorem ipsum lorem ipsumlorem ipsum lorem ipsumlorem ipsumlorem ipsum lorem ipsumlorem ipsum lorem ipsum.lorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsum   lorem ipsum'
})

BlogPost.create({
  title: 'This is title #2',
  content: 'lorem ipsumlorem ipsumlorem ipsum lorem ipsumlorem ipsum lorem ipsumlorem ipsumlorem ipsum lorem ipsumlorem ipsum lorem ipsum.lorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsum   lorem ipsum'
})
